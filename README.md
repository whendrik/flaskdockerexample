# Flask Docker Example

## Building the docker image from dockerfile

We name our image `minimalflask`. 
Note that we do not use the 'EXPOSE', see 
https://docs.docker.com/engine/reference/builder/#expose


```
docker build -t minimalflask:latest -f flask_dockerfile.txt .
```

## Starting the container, with port 5000 forwarded

Forwarding port happens during a `docker run` and cannot be set within dockerfile.

```
docker run -p 5000:5000 minimalflask
```